<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );
class Controller_Welcome extends Controller {
	public function action_index() {
		$this->response->body ( 'hello, world!1' );
	}
	public function action_test() {
		
		// Find user with ID 20
		$user = ORM::factory ( 'TbLine' )->where ( 'line_id', '=', 1 )->find ();
		$v = $user->as_array ();
		echo json_encode ( $v );
	}
} // End Welcome
