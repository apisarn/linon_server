<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );
class Controller_Line extends Controller {
	var $imageUrl = "http://www.linegig.com/upload";
        var $imageDefaultUrl = "http://www.linegig.com/images/logo.png";
        
        var $POST_FROM_APP = 1;
        var $POST_FROM_WEB = 0;
        
	public function action_index() {
		$this->response->body ( 'hello line' );
	}
        
        public function before() {
            $authorized = false;
            if (isset($_SERVER['PHP_AUTH_USER'])) {
                if (strcmp($_SERVER['PHP_AUTH_USER'], 'line-app') == 0 &&
                    strcmp($_SERVER['PHP_AUTH_PW'], 'l1n3@pp') == 0) {
                    $authorized = true;
                }
            }
            if (!$authorized) {
                header('WWW-Authenticate: Basic realm="My Realm"');
                header('HTTP/1.0 401 Unauthorized');
                exit;
            }
        }
        
	public function action_list() {
		
		// get limit
		$limitLength = 5;
		$limit = $this->request->query ( 'limit' );
		if ($limit != null) {
			$limitLength = $limit;
		}
		
		// get sex
		$sexId = 0;
		$sexParam = $this->request->query ( 'sex' );
		if ($sexParam != null) {
			$sexId = $this->getSexId ( $sexParam );
		}
		
		// create where
		$where = "";
		if ($sexId > 0) {
			$where = " line_sex = " . $sexId . " ";
		}
		if (strcmp ( $where, "" ) != 0) {
			$where = " WHERE " . $where;
		}
		
		$query = DB::query ( Database::SELECT, 'SELECT * FROM tb_line l ' . ' LEFT OUTER JOIN province p ' . ' ON p.PROVINCE_ID = l.line_prov ' . $where . ' ORDER BY line_datetime DESC LIMIT ' . $limitLength );
		
		// echo Debug::dump ( $query );
		
		$lines = $query->execute ()->as_array ();
		
		$line_count = count ( $lines );
		
		$list = array ();
		for($i = 0; $i < $line_count; $i ++) {
			
			$response = new Model_Response ();
			$response->id = $lines [$i] ['line_id'];
			$response->code = trim ( $lines [$i] ['line_idno'] );
                        $response->detail = trim ( $lines [$i] ['line_detail'] );
			$image = $lines [$i] ['line_pic'];
			if ($image != null && strcmp ( $image, "" ) > 0) {
				$response->image = $this->imageUrl . "/" . $lines [$i] ['line_pic'];
			} else {
				$response->image = $this->imageDefaultUrl;
			}
			$response->age = $lines [$i] ['line_age'];
			
			$sex = $lines [$i] ['line_sex'];
			
			switch ($sex) {
				case 1 :
					$response->sex = "male";
					break;
				case 2 :
					$response->sex = "female";
					break;
				case 3 :
					$response->sex = "gay";
					break;
				case 4 :
					$response->sex = "tom";
					break;
				default :
					$response->sex = "unknown";
					break;
			}
			$response->province = trim ( $lines [$i] ['PROVINCE_NAME'] );
                        $response->from_app = $lines[$i] ['from_app'];
                        
                        // set datetime back to GMT+0 (so will be compatible to other countries)
                        $datetime = "";
                        $date = new DateTime( $lines [$i] ['line_datetime'] , new DateTimeZone('Asia/Bangkok'));
                        $date->setTimezone(new DateTimeZone('GMT'));
                        $response->datetime = $date->format('Y-m-d H:i:s');
                        
			
			$list [] = $response;
		}
             
		$this->jsonResponse =  json_encode ( $list );
	}
        
        public function action_find() {
            $error = false;
            $response = new Model_Response();
            // get no
            $l_id = $this->request->query ( 'no' );
            if ($l_id == null) {
               $error = true;
            }
            
                       // get no
            $l_detail = $this->request->query ( 'detail' );
            if ($l_detail == null) {
               $error = true;
            }
            
            // get age
            $l_age = $this->request->query ( 'age' );
            if ($l_age == null) {
                $error = true;
            }
            
            // get sex
            $sexParam = $this->request->query ( 'sex' );
            if ($sexParam == null) {
                $error = true;
            } else {
                $l_sex = $this->getSexId ( $sexParam );
            }
            
            // get sex
            $l_province = $this->request->query ( 'province' );
            if ($l_province == null) {
                $error = true;
            }
            
            if ($error) {
                $response->status = 'fail';
                $response->description = 'missing parameters';
            } else {
                // default timezone of this app set to GMT+0
                $date = new DateTime( date("Y-m-d H:i:s") , new DateTimeZone('GMT'));
                $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                $postDate = $date->format('Y-m-d H:i:s');
                
                $insertQuery = DB::insert('tb_line', array('line_idno', 'line_detail', 'line_age', 'line_sex', 'line_prov', 'line_datetime', 'line_ip', 'line_ip_real', 'line_pic', 'mem_id', 'from_app'));
                $insertQuery->values(array($l_id, $l_detail, $l_age, $l_sex, $l_province, $postDate, $this->chk_ip(), $this->chk_ip_real(), '', 0, $this->POST_FROM_APP));
                $line_result = $insertQuery->execute();
                
                $response->status = "ok";
                
                // result[0] is insert id
                $line = new Model_TbLine($line_result[0]);
                
                $response->line = $line->as_array();
                
                $dateReturn = new DateTime( $response->line['line_datetime'], new DateTimeZone('Asia/Bangkok'));
                $dateReturn->setTimezone(new DateTimeZone('GMT'));
                $response->line['line_datetime'] = $dateReturn->format('Y-m-d H:i:s');
            }
             
            $this->jsonResponse =  json_encode ( $response );
        }
        
        public function after() {
            $this->response->headers('Content-Type', 'text/json');
            $this->response->headers('Charset', 'utf-8');
            $this->response->body ($this->jsonResponse);
        }
        
        private function chk_ip(){
	
            if(empty($_SERVER["HTTP_CLIENT_IP"])){ $_SERVER["HTTP_CLIENT_IP"] = "";}
            if(empty($_SERVER["HTTP_X_FORWARDED_FOR"])){ $_SERVER["HTTP_X_FORWARDED_FOR"] = "";}
            if(empty($_SERVER["REMOTE_ADDR"])){ $_SERVER["REMOTE_ADDR"] = "";}
            
            if($_SERVER["HTTP_CLIENT_IP"]){
                    return $_SERVER["HTTP_CLIENT_IP"];
            }else if($_SERVER["HTTP_X_FORWARDED_FOR"]){
                    return $_SERVER["HTTP_X_FORWARDED_FOR"];
            }else{
                return $_SERVER["REMOTE_ADDR"];
            }
		
	}
	
	private function chk_ip_real(){
            return $_SERVER["REMOTE_ADDR"];
	}

	private function getSexId($sex) {
		if (strcmp ( $sex, "male" ) == 0) {
			return 1;
		} elseif (strcmp ( $sex, "female" ) == 0) {
			return 2;
		} elseif (strcmp ( $sex, "gay" ) == 0) {
			return 3;
		} elseif (strcmp ( $sex, "tom" ) == 0) {
			return 4;
		}
		return 0;
	}
} // End Welcome
