<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );
class Controller_Postbox extends Controller {
	var $imageUrl = "http://www.linegig.com/upload";
        var $imageDefaultUrl = "http://www.linegig.com/images/logo.png";
        
        var $POST_FROM_APP = 1;
        var $POST_FROM_WEB = 0;
        
	public function action_index() {
		$this->response->body ( 'hello postbox' );
	}

        public function action_post() {
            $response = new Model_Response();
            $error = false;
            // get from line id
            $from_line_id = $this->request->query ( 'from_line_id' );
            if ($from_line_id == null) {
               $error = true;
            }
            
                       // get no
            $to_line_id = $this->request->query ( 'to_line_id' );
            if ($to_line_id == null) {
               $error = true;
            }
            
            // get age
            $post = $this->request->query ( 'post' );
            if ($post == null) {
                $error = true;
            }
            
            if (strcmp(trim($post), "") <= 0) {
                $error = true;
            }
            
            if (!$this->isLineIdExist($from_line_id)) {
                $error = true;
            }
            
            if (!$this->isLineIdExist($to_line_id)) {
                $error = true;
            }
          
            if ($error) {
                $response->status = 'fail';
                $response->description = 'missing parameters';
            } else {
                // default timezone of this app set to GMT+0
                $date = new DateTime( date("Y-m-d H:i:s") , new DateTimeZone('GMT'));
                $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                $postDate = $date->format('Y-m-d H:i:s');
    
                $insertQuery = DB::insert('b_postbox', array('from_line_id', 'to_line_id', 'post', 'post_date', 'from_app'));
                $insertQuery->values(array($from_line_id, $to_line_id, $post, $postDate, $this->POST_FROM_APP));
                $post_result = $insertQuery->execute();
                
                $response->status = "ok";
                
                // result[0] is insert id
                $postbox = new Model_Postbox($post_result[0]);
                
                $response->postbox = $postbox->as_array();
                
                 // set datetime back to GMT+0 (so will be compatible to other countries)
                $dateReturn = new DateTime( $response->postbox['post_date'], new DateTimeZone('Asia/Bangkok'));
                $dateReturn->setTimezone(new DateTimeZone('GMT'));
                $response->postbox['post_date'] = $dateReturn->format('Y-m-d H:i:s');
            }
            $this->jsonResponse = json_encode($response);
	}
        
        private function isLineIdExist($lineId) {
            $line = new Model_TbLine($lineId);
            return $line->loaded();
        }
        
        public function before() {
            $authorized = false;
            if (isset($_SERVER['PHP_AUTH_USER'])) {
                if (strcmp($_SERVER['PHP_AUTH_USER'], 'line-app') == 0 &&
                    strcmp($_SERVER['PHP_AUTH_PW'], 'l1n3@pp') == 0) {
                    $authorized = true;
                }
            }
            if (!$authorized) {
                header('WWW-Authenticate: Basic realm="My Realm"');
                header('HTTP/1.0 401 Unauthorized');
                exit;
            }
        }
        
        public function after() {
            $this->response->headers('Content-Type', 'text/json');
            $this->response->headers('Charset', 'utf-8');
            $this->response->body ($this->jsonResponse);
        }
}
        