<?php defined('SYSPATH') OR die('No Direct Script Access');

Class Model_TbLine extends ORM
{
	protected $_table_name = 'tb_line';
	protected $_primary_key = 'line_id';
}