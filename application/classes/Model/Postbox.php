<?php defined('SYSPATH') OR die('No Direct Script Access');

Class Model_Postbox extends ORM
{
	protected $_table_name = 'b_postbox';
	protected $_primary_key = 'id';
}